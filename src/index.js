import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Blog from "./Blog/Blog";
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/';

ReactDOM.render(<Blog />, document.getElementById('root'));
registerServiceWorker();
