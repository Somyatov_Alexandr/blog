import React, {Component, Fragment} from 'react';
import axios from 'axios';
import './Blog.css';
import Post from "./Post/Post";
import FullPost from "./FullPost/FullPost";

class Blog extends Component {
  state = {
    posts: [],
    postsFormShown: false,
    selectedPostId: null
  };

  // makeRequest = (url) => {
  //   return fetch(url)
  //       .then(response => {
  //         if (response.ok) {
  //           return response.json();
  //         }
  //         throw new Error('Something went wrong with network request!');
  //       })
  // };

  postSelectedHandler = id => {
    this.setState({selectedPostId: id});
  };

  componentDidMount() {
    const POSTS_URL = 'posts?_limit=3';
    const USER_URL = 'users/';

    axios.get(POSTS_URL)
        .then(posts => {
          return Promise.all(posts.data.map(post => {
            return axios.get(USER_URL + post.userId)
                .then(user => {
                  return {
                      ...post,
                    author: user.data.name
                  }
                })
          })).then(posts => {
            this.setState({posts});
          }).catch(error => {
            console.log(error)
          })
    })
  }

  togglePostsForm = () => {
    this.setState(prevState => {
      return {postsFormShown: !prevState.postsFormShown}
    })
  };

  render() {
    let postsForm = null;

    if (this.state.postsFormShown) {
      postsForm = <section className="NewPost" style={{textAlign: 'center'}}>
        <p>New post form will be here</p>
      </section>
    }
    return (
        <Fragment>
          {postsForm}
          <section className="Posts">
            {this.state.posts.map(post => (
                <Post
                    key={post.id}
                    title={post.title}
                    author={post.author}
                    clicked={() => this.postSelectedHandler(post.id)}
                />
            ))}
          </section>
          <section>
            <FullPost id={this.state.selectedPostId}/>
          </section>
          <button className="ToggleButton" onClick={this.togglePostsForm}>New post</button>
        </Fragment>
  )
  }
}

export default Blog;