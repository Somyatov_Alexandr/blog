import React, {Component} from 'react';
import './FullPost.css';
import axios from "axios";

class FullPost extends Component {

  state = {
    loadPost: null
  };

  componentDidUpdate() {

    if (this.props.id) {
      if (!this.state.loadPost || (this.state.loadPost && this.state.loadPost.id !== this.props.id)) {
        axios.get('posts/' + this.props.id)
            .then(response => {
              this.setState({loadPost: response.data});
            })
      }

    }
  }

  render() {
    if (this.state.loadPost) {
      return (
          <div className="FullPost">
            <h1>{this.state.loadPost.title}</h1>
            <p>{this.state.loadPost.body}</p>
            <div className="Edit">
              <button className="Delete">Delete</button>
            </div>
          </div>
      );
    } else {
      return <p style={{textAlign: 'center'}}>Please select a Post!</p>
    }

  }
}


export default FullPost;
